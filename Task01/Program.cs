﻿using System.ComponentModel.DataAnnotations;

/*   
    1 Вариант 7
    В одномерном массиве, состоящем из N вещественных элементов, вычислить:
    номер минимального элемента массива
    сумму элементов массива, расположенных между первым и вторым отрицательными элементами.
    Преобразовать массив таким образом, чтобы сначала располагались все элементы, модуль которых
    не превышает единицу, а потом — все остальные. 

    2 Вариант 7
    Характеристикой столбца целочисленной матрицы назовем сумму модулей его отрицательных нечетных
    элементов. Переставляя столбцы заданной матрицы, расположить их в соответствии с ростом характеристик.
    Найти сумму элементов в тех столбцах, которые содержат хотя бы один отрицательный элемент.
*/

namespace Task01
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Part 1:");
            Console.Write("Enter array size: ");

            int size = int.Parse(Console.ReadLine());

            var firstPart = new FirstPart(size);

            Console.WriteLine("Original array: ");
            PrintVector(firstPart.Vector);

            Console.WriteLine("Min num: " + firstPart.GetMinNum());

            Console.WriteLine("Negatives sum: " + firstPart.GetSumBetweenNegative());
            firstPart.rearrange();
            Console.WriteLine("Rearranging:");
            PrintVector(firstPart.Vector);

            Console.WriteLine("Part 2:");

            Console.Write("Columns: ");
            size = int.Parse(Console.ReadLine());

            Console.Write("Rows: ");
            int size2 = int.Parse(Console.ReadLine());

            Console.WriteLine("Matrix:");
            var secondPart = new SecondPart(size2, size);
            PrintMatrix(secondPart.Matrix);

            Console.WriteLine("Sorted matrix:");
            secondPart.SortAccordingToACharacteristic();
            PrintMatrix(secondPart.Matrix);

            Console.WriteLine("Sums In Columns With Negatives:");
            secondPart.SumInColumnsWithNegatives();
        }

        static void PrintVector(IEnumerable<double> vector)
        {
            Console.WriteLine(string.Join(" ", vector));
        }

        static void PrintMatrix(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j].ToString().PadLeft(4, ' ') + " ");
                }
                Console.WriteLine();
            }
        }
    }
}