﻿namespace Task01
{
    public class FirstPart
    {
        private readonly double[] array;

        public FirstPart(int length)
        {
            if (length <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length));
            }

            var random = new Random();

            array = new double[length];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = random.NextDouble() * 20 - 10;
            }
        }

        public IReadOnlyList<double> Vector
        {
            get
            {
                return array;
            }
        }

        /*номер минимального элемента массива*/
        public int GetMinNum()
        {
            return Array.IndexOf(array, array.Min()) + 1;
        }

        /*сумму элементов массива, расположенных между первым и вторым отрицательными элементами.*/
        public double GetSumBetweenNegative()
        {
            double sum = 0;
            int first = -1, second = -1, i;

            for (i = 0; i < array.Length; i++)
            {
                if (array[i] < 0)
                    if (first == -1) first = i;
                    else if (second == -1) second = i;
            }

            for (i = first + 1; i < second; i++)
                sum += array[i];

            return sum;
        }

        /*Преобразовать массив таким образом, чтобы сначала располагались все элементы, модуль которых
    не превышает единицу, а потом — все остальные.*/
        public void rearrange()
        {
            int k = 0;
            double a;
            for (int i = 0; i < array.Length; i++)
            {
                if (Math.Abs(array[i]) <= 1)
                {
                    a = array[k];
                    array[k] = array[i];
                    array[i] = a;
                    k++;
                }
            }
        }
    }
}