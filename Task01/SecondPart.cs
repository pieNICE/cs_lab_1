﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task01
{
    public class SecondPart
    {
        private readonly int[,] matrix;

        public SecondPart(int rows, int cols)
        {
            if (rows < 0 || cols < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rows) + " or " + nameof(cols));
            }

            matrix = new int[rows, cols];

            var random = new Random();
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = random.Next(-10, 10);
                }
            }
        }

        public int[,] Matrix
        {
            get
            {
                return matrix;
            }
        }

        /*Характеристикой столбца целочисленной матрицы назовем сумму модулей его отрицательных нечетных
    элементов. Переставляя столбцы заданной матрицы, расположить их в соответствии с ростом характеристик.*/
        public void SortAccordingToACharacteristic()
        {
            int counter = 0;
            int[] mat = new int[matrix.GetLength(1)];

            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    if (matrix[i, j] < 0 && matrix[i, j] % 2 != 0)
                        counter += Math.Abs(matrix[i, j]);
                }
                mat[j] = counter;
                counter = 0;
            }

            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                for (int i = j + 1; i < matrix.GetLength(1); i++)
                {
                    if (mat[j] > mat[i])
                    {
                        for (int k = 0; k < matrix.GetLength(0); k++)
                        {
                            counter = matrix[k, j];
                            matrix[k, j] = matrix[k, i];
                            matrix[k, i] = counter;
                        }
                        counter = mat[j];
                        mat[j] = mat[i];
                        mat[i] = counter;
                    }
                }
            }
        }

        /*Найти сумму элементов в тех столбцах, которые содержат хотя бы один отрицательный элемент.*/
        public void SumInColumnsWithNegatives()
        {
            int sum = 0;
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    if (matrix[i, j] < 0)
                    {
                        for(int k = 0; k < matrix.GetLength(0); k++)
                            sum += matrix[k, j];
                        Console.WriteLine((j + 1) + ":" + sum);
                        sum = 0;
                        break;
                    }
                }
            }
        }

    }
}
