namespace Task01.Tests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(0, -10)]
        public void IsWrongLengthVectorCreatingFailed(int a, int b)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new FirstPart(a));
            Assert.Throws<ArgumentOutOfRangeException>(() => new FirstPart(b));
        }

        [Fact]
        public void IsMinValid()
        {
            var firstPart = new FirstPart(100);
            Assert.Equal(firstPart.Vector[firstPart.GetMinNum() - 1], firstPart.Vector.Min());
        }

        [Fact]
        public void IsSumBetweenFirstTwoNegativesValid()
        {
            var first = -1;
            var second = -1;
            var firstPart = new FirstPart(100);
            var arr = firstPart.Vector.ToArray();
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] < 0)
                {
                    if (first == -1)
                        first = i;
                    else
                        if (second == -1)
                        second = i;
                }
            }
            var sum = 0.0;
            for (int i = first + 1; i < second; i++)
                sum += arr[i];
            Assert.Equal(firstPart.GetSumBetweenNegative(), sum);
        }
    }
}